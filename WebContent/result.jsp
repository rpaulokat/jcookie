<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Cookie-Suchmaschine</title>
<link type="text/css" href="result.css" rel="stylesheet" media="screen" />
<script src="cookie.js" type="text/javascript"></script>

<%@ page import="java.util.Set"%> 
<%@ page import="java.util.ArrayList"%> 
<%@ page import="java.util.Iterator"%> 
<%@ page import="com.gargoylesoftware.htmlunit.util.Cookie" %>
<%@ page import="jcookie.CookieSorter"%> 
<%@ page import="jcookie.JCookie" %>
<%@ page session="true"%>
</head>

<body>
<jsp:useBean id="searchBean" class="jcookie.bean.SearchBean" scope="request" />
<jsp:useBean id="messageBean" class="jcookie.bean.MessageBean" scope="request" />

<div id="page">
<div id="top_nav">
<div id="top_left"><a href="/jcookie/index.jsp">Suche</a> | <a href="/jcookie/about.jsp">Über die Suchmaschine</a></div>
<div id="top_right"><a href="https://www.verbraucher-sicher-online.de" title='Verbraucher Sicher Online'>Zu Verbraucher sicher online</a></div>
<br />
</div>
<div id="header">
<div id="logo"><img src="cookie-logo-2-small.png"
	alt="cookie logo" align="left" /></div>
<form action="/jcookie/cookie_search" method="post"
	onsubmit="return showspinner()">
<input type="text" name='suche' class='search' value='<jsp:getProperty
	property="searchString" name="searchBean" />' /> <input type="submit"
	name='submit' value="Cookies suchen" class='search_button' />
</form>
<br />
</div><!-- END DIV header -->
<div id="content">
<!-- DIV main -->
<div id="main">
<div id='search_header'>

<% if (messageBean.hasErrors()) { %>
<div id='error'>Fehler: <%= messageBean.getErrorMessage() %></div>
<% } if (messageBean.hasMessages()) { %>
<div id='message'>Hinweis: <%= messageBean.getMessage() %></div>
<% } %>
</div><!-- END DIV search_header -->
<%
	CookieSorter csorter = (CookieSorter) request.getAttribute("cookieSorter");
	ArrayList<JCookie> thirdPartyCookies = csorter.getThirdPartyCookies();
	ArrayList<JCookie> domainCookies = csorter.getDomainCookies();
%>
<div id='summary'>
	<p><jsp:getProperty property="resultSize" name="searchBean" /> Cookies gefunden. <%= domainCookies.size() %>  Erstanbieter Cookies, <%= thirdPartyCookies.size() %> Drittanbieter Cookies</p>
</div><!-- END DIV summary -->
<div id="search_results">
<%
	Iterator<JCookie> iter1 = domainCookies.iterator();
	if (iter1.hasNext()) {
%>
<h3 class='result_listing'><%= domainCookies.size() %> &nbsp; Erstanbieter Cookies</h3>
<% } %>
<%
	while (iter1.hasNext()) {
		JCookie cookie = iter1.next();
		String expires;
		String secure;
		if (cookie.getExpires() == null) {
			expires = "Am Ende der Session";
		} else {
			expires = cookie.getExpires().toString();
		}
		if (cookie.isSecure() == false) {
			secure = "Nein";
		} else {
			secure = "Nur gesicherte Verbindungen";
		}
%>
<table>
<caption><%= cookie.getName() %></caption>
<tr><td><em>Wert</em>:</td><td><%= cookie.getValue() %></td></tr>
<tr><td><em>Domain</em>:</td><td><%= cookie.getDomain() %>
<% if (cookie.hasInfo()) { %>
&nbsp; <span class="ggl">Hinweis: </span><a href="cookie_info?domainID=<%=cookie.getInfo() %>" ><%= cookie.getInfo() %></a>
<% } else if (cookie.getName().startsWith("__")) { %>
&nbsp; <span class="ggl">Hinweis: </span><a href="cookie_info?domainID=GoogleAnalytics" >GoogleAnalytics</a>
<% } %>
</td></tr>
<tr><td><em>Verfällt</em>:</td><td><%= expires %></td></tr>
<tr><td><em>Pfad</em>:</td><td><%= cookie.getPath() %></td></tr>
<tr><td><em>Sicher</em>:</td><td><%= secure %></td></tr>
</table>
<% } %>
<%
	Iterator<JCookie> iter2 = thirdPartyCookies.iterator();
	if (iter2.hasNext()) {
%>
<h3 class='result_listing'><%= thirdPartyCookies.size() %> &nbsp; Drittanbieter Cookies</h3>
<% } %>
<%
	while (iter2.hasNext()) {
		JCookie cookie = iter2.next();
		String expires;
		String secure;
		if (cookie.getExpires() == null) {
			expires = "Am Ende der Session";
		} else {
			expires = cookie.getExpires().toString();
		}
		if (cookie.isSecure() == false) {
			secure = "Nein";
		} else {
			secure = "Nur gesicherte Verbindungen";
		}
%>
<table>
<caption><%= cookie.getName() %></caption>
<tr><td><em>Wert</em>:</td><td><%= cookie.getValue() %></td></tr>
<tr><td><em>Domain</em>:</td><td><%= cookie.getDomain() %> 
<% if (cookie.getName().startsWith("__")) { %>
&nbsp; <span class="ggl">Hinweis: </span><a href="cookie_info?domainID=GoogleAnalytics" >GoogleAnalytics</a>
<% } if (cookie.hasInfo()) { %>
&nbsp; <span class="ggl">Hinweis: </span><a href="cookie_info?domainID=<%=cookie.getInfo() %>" ><%= cookie.getInfo() %></a>
<% } %>
</td></tr>
<tr><td><em>Verfällt</em>:</td><td><%= expires %></td></tr>
<tr><td><em>Pfad</em>:</td><td><%= cookie.getPath() %></td></tr>
<tr><td><em>Sicher</em>:</td><td><%= secure %></td></tr>
</table>
<% } %>

</div><!-- END DIV search_results -->
</div><!-- END DIV main -->
<jsp:include page="./_sidebar.jsp" />
<!-- END DIV sidebar --></div>
<!--  END DIV content --> <br />
<div id="footer">
<div class="footer_column">
<ul class="footer_list">
	<li><a href="./about.jsp">Über die Cookiesuche</a></li>
	<li><a href="http://www.verbraucher-sicher-online.de">Verbraucher
	sicher online</a></li>
	<li><a href="./index.jsp">Zur Suche</a></li>
	<li><a href="http://flattr.com/thing/85476/Cookie-Suche"
		target="_blank"> <img
		src="http://api.flattr.com/button/flattr-badge-large.png"
		alt="Flattr this" title="Flattr this" border="0" /> </a></li>
</ul>
</div>
<div class="footer_column">
<a href="http://www.verbraucher-sicher-online.de">
	<img src="versiotheme_logo.png" alt="verbraucher-sicher-online" border="0"/>
</a>
</div>
<div class="footer_column">
<div id="tu_logo"><span class='tiny'>Ein Projekt der</span><br />
<a href="http://www.tu-berlin.de/"> <img
	src="logo-tuberlin.gif" alt="Technische Universität Berlin"
	width="152px" height="72px" border="0"/></a></div>
<br />
</div>
<br />
</div>
</div>
</body>
</html>