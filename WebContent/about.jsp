<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Insert title here</title>
<link type="text/css" href="result.css" rel="stylesheet" media="screen" />
<script src="cookie.js" type="text/javascript"></script>

</head>

<body>

<div id="page">
<div id="top_nav">
<div id="top_left"><a href="/jcookie/index.jsp">Suche</a> | <a href="/jcookie/about.jsp">Über die Suchmaschine</a></div>
<div id="top_right"><a href="https://www.verbraucher-sicher-online.de" title='Verbraucher Sicher Online'>Zu Verbraucher sicher online</a></div>
<br />
</div>
<div id="header">

</div><!-- END DIV header -->
<div id="content">


<div id='center_search'>
<img src="/jcookie/cookie-logo-2.png" alt="cookie" />
</div>


<h2>Über die Cookiesuche</h2>
<p>Zur Benutzung der Cookie-Suche geben Sie einfach eine Webadresse (URL) in das Eingabefeld ein.</p>
<p>Die Cookiesuchmaschine besucht die angegebene Adresse und versucht auszuwerten, 
welche Cookies beim Besuch gesetzt werden. Die Auswertung der eingegebenen Adresse kann mehrere Sekunden dauern. 
Die Darstellung der Ergebnisse erfolgt zweigeteilt in sogenannte Erstanbieter-Cookies und Drittanbieter-Cookies.</p>
<p>Die Suche kann mittels unterschiedlicher Browser-Simulationen durchgeführt werden. 
Hierzu können Sie den gewünschten Browser wählen, und ob dieser eventuell vorhandenes Javascript auf der Zieladresse auswertet.</p>
<p>Auf Basis von: <a href="http://www.java.com">Java</a>, <a href="http://htmlunit.sourceforge.net/">HtmlUnit</a>, <a href="http://tomcat.apache.org/">Tomcat</a></p>
<p>Die Cookiesuchmaschine verwendet den Einsatz von Session-Cookies, um die evtl. gemachten Einstellungen bzgl. Browser und Javascript für die Dauer des Besuchs zu speichern. Nach Ablauf ihrer Sitzung werden diese Cookies automatisch von ihrem Browser gelöscht.</p>


</div><!--  END DIV content --> 
<br />
<%@ include file="./_footer.jsp" %>
</div>
</body>
</html>