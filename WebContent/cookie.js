function hidespinner() {
	if (document.getElementById) { // DOM3 = IE5, NS6
		document.getElementById('spinner').style.visibility = 'hidden';
	} else {
		if (document.layers) { // Netscape 4
			document.spinner.visibility = 'hidden';
		} else { // IE 4
			document.all.spinner.style.visibility = 'hidden';
		}
	}
}

function showspinner() {
	if (document.getElementById) { // DOM3 = IE5, NS6
		document.getElementById('spinner').style.visibility = 'visible';
	} else {
		if (document.layers) { // Netscape 4
			document.spinner.visibility = 'visible';
		} else { // IE 4
			document.all.spinner.style.visibility = 'visible';
		}
	}
}


function showElement(id) {
	document.getElementById(id).style.visibility = 'visible';
	document.getElementById(id).style.display = 'block';
	/* document.getElementById(id).setAttribute('display', 'block'); */
}

function hideElement(id) {
	document.getElementById(id).style.visibility = 'hidden';
	document.getElementById(id).style.display = 'none';
	/* document.getElementById(id).setAttribute('display', 'none'); */
}
