<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Cookie-Informationen</title>
<link type="text/css" href="result.css" rel="stylesheet" media="screen" />
<script src="cookie.js" type="text/javascript"></script>
<%@page import="jcookie.bean.InfoBean" %>
</head>
<body>

<div id="page">
<div id="top_nav">
<div id="top_left"><a href="/jcookie/index.jsp">Suche</a> | <a href="/jcookie/about.jsp">Über die Suchmaschine</a></div>
<div id="top_right"><a href="https://www.verbraucher-sicher-online.de" title='Verbraucher Sicher Online'>Zu Verbraucher sicher online</a></div>
<br />
</div>
<div id="header">
<div id="logo"><img src="cookie-logo-2-small.png"
	alt="cookie logo" align="left" /></div>
<form action="/jcookie/cookie_search" method="post"
	onsubmit="return showspinner()">
<input type="text" name='suche' class='search' value='' /> <input type="submit"
	name='submit' value="Cookies suchen" class='search_button' />
</form>
<br />
</div>
<!-- END DIV header -->

<div id="content">
<!-- DIV main -->
<div id="main">
<jsp:useBean id="infoBean" class="jcookie.bean.InfoBean" scope="request" />

<div id="info_table">
<jsp:include page="<%= infoBean.getPartial() %>"/>
</div>

<a href="javascript:history.back()">&laquo; zurück</a>
</div><!-- END DIV main -->
<jsp:include page="./_sidebar.jsp" />
<!-- END DIV sidebar --></div>
<!--  END DIV content --> <br />
<div id="footer">
<div class="footer_column">
<ul class="footer_list">
	<li><a href="./about.jsp">Über die Cookiesuche</a></li>
	<li><a href="http://www.verbraucher-sicher-online.de">Verbraucher
	sicher online</a></li>
	<li><a href="./index.jsp">Zur Suche</a></li>
	<li><a href="http://flattr.com/thing/85476/Cookie-Suche"
		target="_blank"> <img
		src="http://api.flattr.com/button/flattr-badge-large.png"
		alt="Flattr this" title="Flattr this" border="0" /> </a></li>
</ul>
</div>
<div class="footer_column">
<a href="http://www.verbraucher-sicher-online.de">
	<img src="versiotheme_logo.png" alt="verbraucher-sicher-online" border="0"/>
</a>
</div>
<div class="footer_column">
<div id="tu_logo"><span class='tiny'>Ein Projekt der</span><br />
<a href="http://www.tu-berlin.de/"> <img
	src="logo-tuberlin.gif" alt="Technische Universität Berlin"
	width="152px" height="72px" border="0"/></a></div>
<br />
</div>

<br />
</div>
</div>
</body>
</html>