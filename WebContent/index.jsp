<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
       "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page session="true" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
  <title>Cookie-Suchmaschine</title>
  <link type="text/css" href="/jcookie/result.css" rel="stylesheet"  />
  <script src="cookie.js" type="text/javascript"></script>
</head>
<body>
<jsp:useBean id="messageBean" class="jcookie.bean.MessageBean" scope="request" />
<div id='page'>
<div id="top_nav">
<div id="top_left"><a href="/jcookie/index.jsp">Suche</a> | <a href="/jcookie/about.jsp">Über die Suchmaschine</a></div>
<div id="top_right"><a href="https://www.verbraucher-sicher-online.de" title='Verbraucher Sicher Online'>Zu Verbraucher sicher online</a></div>
<br />
</div>

<div id='content'>
		<div id='center_search'>
			<div id='logo'><img src="/jcookie/cookie-logo-2.png" alt='cookie' /></div>
			<form action="/jcookie/cookie_search" method="post" onsubmit="return showspinner()">
  			<input type="text" name='suche' class='search' /><br />
	 		<input type="submit" name='submit' value="Cookies suchen" class='search_button' />
	 		</form>
	 		<%  if (messageBean.hasErrors()) {
			out.println("<div id='error'>Fehler: " + messageBean.getErrorMessage() + "</div>");
			}
			if (messageBean.hasMessages()) {
		 	out.println("<div id='message'>Hinweis: " + messageBean.getMessage() + "</div>");
			} %>
			<div id='spinner'>
				<p>Bitte haben sie einige Sekunden Geduld!</p>
				<img src='/jcookie/spinner.gif' alt='bitte warten!' />
			</div>
		</div>
</div>
<br />	
 	<div id='footer'> 	
 	 <div id="footer_center">
 	 <span class='footer_big'>Präsentiert von:</span><a href="http://www.verbraucher-sicher-online.de"><img src="/jcookie/versiotheme_logo.png" alt="verbraucher sicher online" class='img_middle' border=0 /></a>
 	 </div>
 	</div>

</div>
</body>
</html>
