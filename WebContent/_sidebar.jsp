
<div id="sidebar">

<div id='settings'>
<div class="boxed">
<form action="/jcookie/search_setting" method="post"
	onsubmit="return showspinner()">
<label for='browser_string'>Browser:</label>
<jsp:useBean id="optionBean" class="jcookie.bean.OptionBean" scope="session" />
<select name='browser'>
	<option id="IE6" value="IE6"<%=
	optionBean.browserSelected("IE6") %>>IE6</option>
	<option id="IE7" value="IE7"<%=
	optionBean.browserSelected("IE7") %>>IE7</option>
	<option id="IE8' value="IE8" <%=
	optionBean.browserSelected("IE8") %>>IE8</option>
	<option id="Firefox2" value="FF3"<%=
	optionBean.browserSelected("FF3") %>>Firefox 3</option>
	<option id="Firefox3" value="FF36"<%=
	optionBean.browserSelected("FF36") %>>Firefox 3.6</option>
</select>
<br />
Javascript:
<input type="radio" name="javascript" value="1"<%= optionBean.javascriptChecked() %> > on &nbsp; <input type="radio" name="javascript" value="0"<%= optionBean.javascriptUnChecked()%>> off<br />
<br />
<input type='submit' value='Einstellungen ändern' />
</form>
</div><!-- END DIV boxed -->
</div><!-- END DIV settings -->


Was sind:&nbsp;
<em class='underlined'>Erstanbieter Cookies</em>
<div id="1st">&raquo; Sind Cookies, die von der besuchten Website gesetzt werden.
Bei einem Besuch der Webseite www.verbraucher-sicher-online.de wird bspw. ein Session Cookie gesetzt,
 welches f&uuml;r den Verlauf des Besuches festh&auml;lt, ob ihr Browser Javascript unters&uuml;tzt.
Dieses Cookie ist nur g&uuml;ltig f&uuml;r die Domain verbraucher-sicher-online.de. 
Mit einem Besuch bei verbraucher-sicher-online.de wird also ein Cookie dieser Domain aus &raquo;erster Hand&laquo; gesetzt.
</div><br />
Was sind: &nbsp;
<em class='underlined'>Drittanbieter Cookies</em>
<div id="3rd">&raquo; Eine Webseite ist aus verschiedenen Elementen  zusammengesetzt.
Der Speicherort f&uuml;r ein Bild muss nicht auf dem Server der aufgerufenen Webseite sein.
Es kann von einem andren Ort stammen, 
bzw. von dort automatisch vom Browser angefordert werden.
Die Anforderung dieses spezifischen Teiles der Webseite kann dazu f&uuml;hren, 
dass der ausliefernde Webserver ein Cookie setzen m&ouml;chte. Ein sog. Drittanbieter Cookie.
Damit kann eine dritte Partei Informationen &uuml;ber Ihren Surfverlauf sammeln.</div>
<br />


</div><!-- END DIV sidebar -->
