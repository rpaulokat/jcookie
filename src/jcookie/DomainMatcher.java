package jcookie;

import java.util.HashMap;


public class DomainMatcher {
	
	
	/**
	 * 
	 * @param dom domain to be looked up
	 * @return true|false on success|failure
	 */
	public static boolean includes(String dom) {
		if (createHash().containsKey(dom)) {
			return true;
		} else {
			return false;
		}
	}
	
	
	/**
	 * 
	 * @param dom domain to lookup
	 * @return String on success or null if not existing
	 */
	public static String get(String dom) {
		HashMap<String,String> map = createHash();
		if (map.containsKey(dom) ) {
			return map.get(dom);
		} else {
			return null;
		}
	}
	
	/**
	 * 
	 * @return HashMap with domain->company-id fields
	 */
	public static HashMap<String,String> getHashMap() {
		return createHash();
	}

	/**
	 * creates lookup-table to get information about domain -> company_id relations
	 * @return
	 */
	private static HashMap<String, String> createHash() {

		HashMap<String, String> domainHash = new HashMap<String, String>();

		domainHash.put("adition.com", "Adition");
		domainHash.put("adviva.net", "SpecificMedia");
		domainHash.put("advolution.de", "Advolution");
		domainHash.put("affimax.de", "DMWD");
		domainHash.put("amgdgt.com", "Adconion");
		domainHash.put("doubleclick.net", "DoubleClick");
		domainHash.put("fastclick.net", "ValueClick");
		domainHash.put("google-analytics.com", "GoogleAnalytics");
		domainHash.put("imrworldwide.com", "Nielsen");
		domainHash.put("ivwbox.de", "IVW-INFOnline");
		domainHash.put("ligatus.com", "Ligatus");
		domainHash.put("metalyzer.com", "Metapeople");
		domainHash.put("newtention.net", "Newtention");
		domainHash.put("nuggad.net", "Nugg.ad");
		domainHash.put("onad.eu", "ONAD");
		domainHash.put("orangeuser.com", "Net-Publics");
		domainHash.put("quality-channel.de", "Spiegel-QC");
		domainHash.put("quisma.com", "Quisma");
		domainHash.put("serving-sys.com", "Mediamind");
		domainHash.put("sitestat.com", "Nedstat");
		domainHash.put("smartadserver.com", "SMARTAdServer");
		domainHash.put("specificclick.net", "SpecificMedia");
		domainHash.put("tradedoubler.com", "TradeDoubler");
		domainHash.put("uimserv.net", "1und1");
		domainHash.put("webmasterplan.com", "Affilinet");
		domainHash.put("webtrekk.net", "Webtrekk");
		domainHash.put("wunderloop.net", "AudienceScience");
		domainHash.put("yieldmanager.com", "RightMedia-Yahoo");
		domainHash.put("zanox.com", "Zanox");
		domainHash.put("zanox-affiliate.de", "Zanox");
		
		return domainHash;
	}

}
