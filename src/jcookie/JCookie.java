package jcookie;


import java.util.Date;

import com.gargoylesoftware.htmlunit.util.Cookie;


/**
 * Extension for htmlunit.util.Cookie - to be able to hold information 
 * about given Cookie. Copies instance Values of Cookie cause Class Cookie is immutable
 * 
 * @author rp
 * 
 */
public class JCookie  {

	private Cookie cookie;

	public String info;
	
	private String domain;
	private String name;
	private String value;
	private boolean secure;
	private String path;
	private Date expires;
	
	/**
	 * Constructor 
	 * @param cookie com.gargoylesoftware.htmlunit.util.Cookie
	 */
	public JCookie(Cookie cookie) {
		this.cookie = cookie;
		copyValues();
	}

	
	/**
	 * copies instance-values of immutable class Cookie
	 * 
	 */
	private void copyValues() {
		this.domain = this.cookie.getDomain();
		this.name = this.cookie.getName();
		this.value = this.cookie.getValue();
		this.secure = this.cookie.isSecure();
		this.path = this.cookie.getPath();
		this.expires = this.cookie.getExpires();
	}


	/**
	 * checks if given JCookie has extra-info
	 * @return true if info or false if not
	 */
	public boolean hasInfo() {
		if ( (info != null) && (info.length() > 1)) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Returns Info-String for given JCookie
	 * @return String
	 */
	public String getInfo() {
		if (hasInfo()) {
			return info;
		} else {
			return "";
		}
	}
	
	
	/**
	 * Sets info String for this JCookie
	 * @param info String
	 */
	public void setInfo(String cookieinfo) {
		info = cookieinfo;
	}
	
	/**
	 * @return the domain
	 */
	public String getDomain() {
		return domain;
	}


	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}


	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}


	/**
	 * @return the secure
	 */
	public boolean isSecure() {
		return secure;
	}


	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}


	/**
	 * @return the expires
	 */
	public Date getExpires() {
		return expires;
	}

}
