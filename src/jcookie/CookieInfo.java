package jcookie;

import java.io.IOException;
import java.io.File;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.*;

import jcookie.bean.InfoBean;

/**
 * Servlet implementation class CookieInfo
 */
public class CookieInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String partialDir = "partials";
	//private String docRoot;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CookieInfo() {
    	super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String docRoot = getServletContext().getRealPath("/");
		
		String domainID = request.getParameter("domainID").trim();
		
		String filepath = partialDir.concat("/").concat(domainID).concat(".html");
		
		String realpath = docRoot + filepath;
		File f = new File(docRoot + filepath);
		//f.getPath();
		
		System.out.println("CookieInfo - filepath: " + filepath);
		
		InfoBean infoBean = new InfoBean(domainID, filepath);
		
		
		if (f.exists()) {
			infoBean.setInfoAvailable(true);
			infoBean.setPartial(filepath);
		} else {
			infoBean.setInfoAvailable(false);
		}
		
		request.setAttribute("infoBean",infoBean);
		
		String forwardUrl = "/domain_info.jsp";
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(forwardUrl);
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
