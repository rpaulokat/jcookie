package jcookie;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jcookie.bean.OptionBean;
import jcookie.bean.SearchBean;

/**
 * Servlet implementation class SearchSetting
 */
public class SearchSetting extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchSetting() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		SearchBean searchBean = new SearchBean();
	
		String _browser = request.getParameter("browser").trim();
		int _javascript = Integer.parseInt(request.getParameter("javascript").trim());
		log("SEARCH_SETTING: browser: [" + _browser + "] - javascript: " + _javascript );
		
		OptionBean option = (OptionBean) request.getSession(true).getAttribute("optionBean");
		option.setBrowserString(_browser);
		option.setJavascriptEnabled(_javascript);
		log("setting javascript to: '" + _javascript + "'");
		
		request.getSession().setAttribute("optionBean", option);
		
		String forwardUrl = "/cookie_search";
		
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(forwardUrl);
		
		dispatcher.forward(request, response);
	}

}
