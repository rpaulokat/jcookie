package jcookie;

import java.io.IOException;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jcookie.bean.MessageBean;
import jcookie.bean.OptionBean;
import jcookie.bean.SearchBean;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.util.Cookie;

/**
 * Servlet implementation class CookieSearch
 */
public class CookieSearch extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private MessageBean messageBean;
	private SearchBean searchBean;
	private String requestedDomain = "none";

	private CookieSorter cSorter;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CookieSearch() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		searchBean = new SearchBean();
		messageBean = new MessageBean();
		CookieSorter cSorter;
		OptionBean option;
		
		String forwardUrl = "/index.jsp";
		String _suche;
		
		/* if this is not a 'post' - we take the searchString from session */
		if (request.getParameter("suche") == null) {
			_suche = (String) request.getSession(true).getAttribute("searchString");
		} else {
			/* otherwise this is a regular post - so we fetch the parameter */
			_suche = request.getParameter("suche").trim();
		}
		
		if (request.getSession(true).getAttribute("optionBean") == null) {
			log("keine optionbean in session - erstelle eine");
			option = new OptionBean();
			request.getSession(true).setAttribute("optionBean", option);
		} else {
			option = (OptionBean) request.getSession().getAttribute("optionBean");
			log("optionbean in session vorhanden - verwende diese");
		}
		
		request.getSession().setAttribute("searchString", _suche);
		
		try {
			String urlString = checkUrlString(_suche);
			log("URLSTRING: " + urlString);
			
			if (urlString.length() > 0) {
				WebClient webClient = new WebClient(option.getBrowserVersion());
				webClient.setJavaScriptEnabled(option.isJavaScriptSelected());
				webClient.getCookieManager().setCookiesEnabled(true);
				webClient.setThrowExceptionOnScriptError(false);
				//log("Browser: " + webClient.getBrowserVersion().getNickname());
				
				HtmlPage page = webClient.getPage(urlString);
				
				//
				
				String xPathExpression = "//*[name() = 'img' or name() = 'link' and @type = 'text/css']";
		        List<?> resultList = page.getByXPath(xPathExpression);
		        	
		        Iterator<?> i = resultList.iterator();
		        while (i.hasNext()) {
		            try {
		                HtmlElement el = (HtmlElement) i.next();

		                String path = el.getAttribute("src").equals("")?el.getAttribute("href"):el.getAttribute("src");
		                if (path == null || path.equals("")) continue;

		                URL url = page.getFullyQualifiedUrl(path);
		                log("FETCHING url: " + url);
		                
		                webClient.getPage(url);
		                
		            } catch (Exception e) {
		            	log("ERROR in while: " + e.getMessage());
		            }
		        }
				
				Set<Cookie> cookies = webClient.getCookieManager().getCookies();
				
				
				this.cSorter = new CookieSorter(cookies, this.requestedDomain);
				ArrayList<JCookie> thirdParty = this.cSorter.getThirdPartyCookies();
					
				Iterator<JCookie> iter = thirdParty.iterator();
				while(iter.hasNext()) {
					log("\nINFO: - ITERATOR: " + iter.next().getDomain() + "\n");
				}
					
				
				
				request.setAttribute("cookieSorter",this.cSorter);
				
				searchBean.setSearchString(urlString);
				searchBean.setResultSize(cookies.size());
				webClient.closeAllWindows();
				forwardUrl = "/result.jsp";
			} else {
				messageBean.setErrorMessage("Keine brauchbare Adresse!");
				forwardUrl = "/index.jsp";
			}
				
		} catch (UnknownHostException e) {
			messageBean.setErrorMessage("Kann Host nicht aufloesen! " + e.toString());
		} catch (Exception e) {
			log("ERROR: " + e.toString());
		}
		
		request.setAttribute("searchBean" , searchBean);
		request.setAttribute("messageBean", messageBean);
		
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(forwardUrl);
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request,response);
	}
	
	private String checkUrlString(String text) throws UnknownHostException {
		
		URL toSearch;
		String _tmp = "";
		
		try {
			if ( (text.startsWith("http://")) || ( text.startsWith("https://") ) ) {
				_tmp = text;
			} else {
				_tmp = "http://".concat(text);
			}
			toSearch = new URL(_tmp);
			String host = toSearch.getHost();
			InetAddress.getByName(host);
			setRequestedDomain(host);
			return toSearch.toString();
		} catch ( MalformedURLException e ) {
			toSearch = null;
			log("\nFEHLER mit: " + text + "\n" + e.getMessage());
		}
		
		return "";
	}
	
	/*
	 * sets necessary domain-part of request beeing made
	 */
	private void setRequestedDomain(String hostadress) {
		String[] arr = hostadress.split("\\.");
		this.requestedDomain = arr[arr.length-2].concat("." + arr[arr.length-1]);
		log("\nINFO: requestedDomain " + this.requestedDomain + "\n");
	}
	

}
