
package jcookie.bean;

/**
 * 
 * @author rp
 * simple dataholder to be used on jsp for display
 */
public class SearchBean {
	
	private String searchString = "Kein Suchbegriff gegeben!";
	private int resultSize = 0;
	

	/**
	 * 
	 * @param term - the searchterm we were lookin for
	 */
	public void setSearchString(String term) {
		searchString = term;
	}
	
	/**
	 * 
	 * @return String - the searchstring 
	 */
	public String getSearchString() {
		return searchString;
	}
	
	/**
	 * 
	 * @return int resultSize - num results
	 */
	public int getResultSize() {
		return this.resultSize;
	}
	
	/**
	 * 
	 * @param i - int length of result
	 */
	public void setResultSize(int i) {
		this.resultSize = i;
	}
	
	
}
