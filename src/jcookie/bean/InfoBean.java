/**
 * POJO that maintains information about 
 * the partial and domainId to be displayed on 
 * jsp-page 
 */

package jcookie.bean;

import java.io.File;

//import java.io.File;

public class InfoBean {
	
	private String domainId;
	private String partial;
	private boolean infoAvailable;
	
	public InfoBean() {
		/* empty constructor */
	}
	
	
	public InfoBean(String domainId) {
		this.domainId = domainId;
	}
	
	public InfoBean(String domainId,String partial) {
		
		this.domainId = domainId;
		this.partial = partial;
		if (! testForExistingPartial(partial) == true) {
			/* TODO */
		}
		
	}
	
	
	
	/**
	 * @return the domainId
	 */
	public String getDomainId() {
		return domainId;
	}

	/**
	 * @param domainId the domainId to set
	 */
	public void setDomainId(String domainId) {
		this.domainId = domainId;
	}

	
	/**
	 * @return the partial
	 */
	public String getPartial() {
		return partial;
	}

	/**
	 * @param partial the partial to set
	 */
	public void setPartial(String partial) {
		this.partial = partial;
	}
	
	
	/**
	 * @return the infoAvailable
	 */
	public boolean isInfoAvailable() {
		return infoAvailable;
	}

	/**
	 * @param infoAvailable the infoAvailable to set
	 */
	public void setInfoAvailable(boolean infoAvailable) {
		this.infoAvailable = infoAvailable;
	}
	
	
	/**
	 * 
	 * @param partial the partial to test if existing
	 * @return true on success false on failure or not existing partial
	 */
	private boolean testForExistingPartial(String partial) {
		
		return true;
	}

	


}
