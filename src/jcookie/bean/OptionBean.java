/**
 * 
 */
package jcookie.bean;

import com.gargoylesoftware.htmlunit.BrowserVersion;

/**
 * @author rp
 * represents search-settings 
 */
public class OptionBean {
	
	private String browserString;
	private int javascriptEnabled;
	
	
	public String browserSelected(String browser) {
		if (browser.equalsIgnoreCase(browserString) ){
			return "selected";
		} else {
			return "";
		}
	}
	
	public OptionBean() {
		this.browserString = "FF36";
		this.javascriptEnabled = 1;
	}
	
	/**
	 * @return the browserString
	 */
	public String getBrowserString() {
		return this.browserString;
	}
	/**
	 * @param browserString the browserString to set
	 */
	public void setBrowserString(String browserString) {
		this.browserString = browserString;
	}
	/**
	 * @return the javascriptEnabled
	 */
	public int getJavascriptEnabled() {
		return this.javascriptEnabled;
	}
	/**
	 * @param javascriptEnabled the javascriptEnabled to set
	 */
	public void setJavascriptEnabled(int javascriptEnabled) {
		if (javascriptEnabled == 1) {
			this.javascriptEnabled = 1;
		} else {
			this.javascriptEnabled = 0;
		}
		
	}
	
	/**
	 * 
	 * @return BrowserVersion - the BrowserVersion to be used by WebClient
	 */
	public BrowserVersion getBrowserVersion() {
		BrowserVersion bv;
		if (this.browserString == "IE6") {
			bv =  BrowserVersion.INTERNET_EXPLORER_6;
		} else {
			if (this.browserString == "IE7") {
				bv =  BrowserVersion.INTERNET_EXPLORER_7;
			} else {
				if (this.browserString == "IE8") {
					bv =  BrowserVersion.INTERNET_EXPLORER_8;
				} else {
					if (this.browserString == "FF3") {
						bv =  BrowserVersion.FIREFOX_3;
					} else {
						if (this.browserString == "FF36") {
							bv = BrowserVersion.FIREFOX_3_6;
						} else {
							bv = BrowserVersion.FIREFOX_3_6;
						}
					}
				}
			}
		}
		return bv;
	}
	
	/**
	 * 
	 * @return boolean if javascript is enabled
	 */
	public boolean isJavaScriptSelected() {
		boolean enabled;
		if (this.javascriptEnabled == 1) {
			enabled = true;
		} else {
			enabled = false;
		}
		return enabled;
	}
	
	public String javascriptChecked() {
		if (isJavaScriptSelected()) {
			return "checked";
		} else {
			return "";
		}
	}
	
	public String javascriptUnChecked() {
		if (isJavaScriptSelected()) {
			return "";
		} else {
			return "checked";
		}
	}
	
	
	

}
