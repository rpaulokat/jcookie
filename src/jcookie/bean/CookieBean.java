/**
 * 
 */
package jcookie.bean;

import java.util.ArrayList;
import java.util.Set;

/**
 * @author rene@so36.net
 *
 */
public class CookieBean {

	private String domain;
	private String path;
	private String validUntil;
	private String comment;
	private boolean secure;
	private Set cookies;
	
	
	public CookieBean() {
	}
	
	public CookieBean(Set cookies) {
		this.cookies = cookies;
	}
	
	/**
	 * @return the secure
	 */
	public boolean isSecure() {
		return secure;
	}
	/**
	 * @param secure the secure to set
	 */
	public void setSecure(boolean secure) {
		this.secure = secure;
	}
	private String name;
	/**
	 * @return the domain
	 */
	public String getDomain() {
		return domain;
	}
	/**
	 * @param domain the domain to set
	 */
	public void setDomain(String domain) {
		this.domain = domain;
	}
	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}
	/**
	 * @param path the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}
	/**
	 * @return the validUntil
	 */
	public String getValidUntil() {
		return validUntil;
	}
	/**
	 * @param validUntil the validUntil to set
	 */
	public void setValidUntil(String validUntil) {
		this.validUntil = validUntil;
	}
	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}
	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	
}
