package jcookie.bean;

public class MessageBean {
	
	private String errorMessage = "";
	private String message = "";
	
	public String getErrorMessage() {
		return this.errorMessage;
	}
	
	public String getMessage() {
		return this.message;
	}
	
	public void setErrorMessage(String text) {
		this.errorMessage = text;
	}
	
	public void setMessage(String text) {
		this.message = text;
	}
	
	public boolean hasErrors() {
		if (this.errorMessage.length() > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean hasMessages() {
		if (this.message.length() > 0 ) {
			return true;
		} else {
			return false;
		}
	}

}
